import { Fragment } from 'react';

import Head from 'next/head';
import { Group, Tabs, Schedule, Heading } from '../components';

import { TABS_LIST, GROUP_LIST, SCHEDULE } from '../utils/variables';

export default function Home() {
  return (
    <div>
      <Head>
        <title>BEEINTERNS</title>
        <meta name="description" content="BEEINTERNS" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Heading />

      <main style={{ padding: '20px', boxSizing: 'border-box' }}>
        <div style={{ maxWidth: '960px', margin: '0 auto', }}>
          <Tabs>
            <Fragment>
              <h2 style={{
                fontFamily: 'Jost',
                fontSize: '56px',
              }}>{TABS_LIST[0]}</h2>
              <Schedule list={SCHEDULE} />
            </Fragment>


            <Fragment>
              <h2 style={{
                fontFamily: 'Jost',
                fontSize: '56px',
              }}>{TABS_LIST[1]}</h2>
              <Group list={GROUP_LIST} />
            </Fragment>
          </Tabs>
        </div>
      </main>

      <footer>

      </footer>
    </div>
  )
}
