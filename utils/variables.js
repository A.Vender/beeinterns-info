export const TABS_LIST = ['Расписание', 'Группы'];

export const SCHEDULE = [{
  "date": "19.07.2022",
  "time": "18:00",
  "mentor": "Олег Скирюк",
  "lecture": "Знакомство с современными инструментами разработки: npm, package.json, babel, eslint, stylelint, webpack, react-scripts",
  "tglink": "https://t.me/beeinterns2022"
}, {
  "date": "21.07.2022",
  "time": "18:00",
  "mentor": "Светлана Митюхина",
  "lecture": "Знакомство с реакт: CRA, jsx на примере классовых компонентов и их базового life cycle (constructor, render, cdm, cdu, cwu). Первичный рендер, обновление, размонтирование, side-эффекты",
  "tglink": "https://t.me/beeinterns2022"
}, {
  "date": "26.07.2022",
  "time": "18:00",
  "mentor": "Анастасия Грязева",
  "lecture": "Работа с props, state, prop-types, особенности children и key и className, render Props, fragment, dangerouslySetInnerHtml",
  "tglink": "https://t.me/beeinterns2022"
}, {
  "date": "28.07.2022",
  "time": "18:00",
  "mentor": "Гайсар Давлеткильдин",
  "lecture": "Обработка событий в React компонентах",
  "tglink": "https://t.me/beeinterns2022"
}, {
  "date": "02.08.2022",
  "time": "18:00",
  "mentor": "Иван Щедрин",
  "lecture": "Внутреннее устройство реакта: Virtual DOM, pure function, shouldComponentUpdate (и PureComponent), знакомство с React Dev Tools",
  "tglink": "https://t.me/beeinterns2022"
}, {
  "date": "04.08.2022",
  "time": "18:00",
  "mentor": "Анастасия Клименко",
  "lecture": "Продвинутые React API: Context, Children API, Refs, cloneElement, Strict Mode",
  "tglink": "https://t.me/beeinterns2022"
}, {
  "date": "09.08.2022",
  "time": "18:00",
  "mentor": "Павел Захаров",
  "lecture": "Hooks и Функциональное программирование, мемоизация",
  "tglink": "https://t.me/beeinterns2022"
}, {
  "date": "11.08.2022",
  "time": "18:00",
  "mentor": "Михаил Сапицын",
  "lecture": "Углубленное изучение классовых компонентов: getDerivedStateFromProps, getSnapshotBeforeUpdate, error boundary, findDomNode",
  "tglink": "https://t.me/beeinterns2022"
}, {
  "date": "16.08.2022",
  "time": "18:00",
  "mentor": "Андрей Сухов",
  "lecture": "Глобальный менеджер состояний: @redux/toolkit, синхронные и асинхронные изменения, знакомство с Redux Dev Tools",
  "tglink": "https://t.me/beeinterns2022"
}, {
  "date": "18.08.2021",
  "time": "18:00",
  "mentor": "Ермолаев Глеб",
  "lecture": "Устройство react-router, history",
  "tglink": "https://t.me/beeinterns2022"
}, {
  "date": "23.08.2021",
  "time": "18:00",
  "mentor": "Иван Малюгин",
  "lecture": "Серверный рендеринг, особенности, частые ошибки, Suspence, lazy Loading, transition",
  "tglink": "https://t.me/beeinterns2022"
}, {
  "date": "25.08.2021",
  "time": "18:00",
  "mentor": "Анонимус",
  "lecture": "Бонус лекция",
  "tglink": "https://t.me/beeinterns2022"
}];

export const GROUP_LIST = [{
  "mentors": [{
    "name": "Ермолаев Глеб",
    "position": "Senior Frontend-разработчик"
  }, {
    "name": "Давлеткильдин Гайсар",
    "position": "Senior Frontend-разработчик"
  }],
  "tglink": "https://t.me/+4qCT8FKFLVlkZDRi",
  "interns": [
    "Евгений Коротĸов",
    "Ильназ Назыров",
    "Людмила Хатанзейская",
    "Дмитрий Черемухин",
    "Вячеслав Тараканов",
    "Юлия Кирюшина",
    "Валентина Гурина"
  ]
}, {
  "mentors": [{
    "name": "Митюхина Светлана",
    "position": "Middle Frontend-разработчик"
  }, {
    "name": "Сухов Андрей",
    "position": "Senior Frontend Team Lead"
  }],
  "tglink": "https://t.me/+NqJhH8HDPckxNmVi",
  "interns": [
    "Арина Киреева",
    "Алеĸсандр Ниĸитин",
    "Алеĸсандр Чеботарев",
    "Константин Абрикосов",
    "Валентина Мазярова",
    "Полина Гайкова",
    "Александра Джулай",
    "Елена Воробьева"
  ]
}, {
  "mentors": [{
    "name": "Щедрин Иван",
    "position": "Senior Frontend-разработчик"
  }, {
    "name": "Грязева Анастасия",
    "position": "Senior Frontend-разработчик"
  }],
  "tglink": "https://t.me/+1KWjugeYsYZjODYy",
  "interns": [
    "Маĸсим Козаĸевич",
    "Георгий Жиглов",
    "Максим Воликов",
    "Алексей Бураков",
    "Виктория Гаврилова",
    "Надежда Мурдашева",
    "Георгий Михалевский",
    "Михаил Бабаянц",
    "Ксения Куренкова"
  ]
}, {
  "mentors": [{
    "name": "Олег Скирюк",
    "position": "Senior Frontend Team Lead"
  }, {
    "name": "Сапицын Михаил",
    "position": "Senior Frontend-разработчик"
  }, {
    "name": "Малюгин Иван",
    "position": "Senior Frontend Team Lead"
  }],
  "tglink": "https://t.me/+PKtx1rIE8t9iMGE6",
  "interns": [
    "Маргарита Худяĸова",
    "Анна Силич",
    "Максим Кузинов",
    "Даниил Беляев",
    "Валерия Трефилова",
    "Никита Русаков",
    "Илья Французов",
    "Ирина Чупракова",
    "Кирилл Журавлёв",
    "Владимир Ефимов"
  ]
}, {
  "mentors": [{
    "name": "Павел Захаров",
    "position": "Senior Frontend-разработчик"
  }, {
    "name": "Клименко Анастасия",
    "position": "Senior Frontend-разработчик"
  }],
  "tglink": "https://t.me/+NW0knxisdMVlMmUy",
  "interns": [
    "Глеб Туржинсĸий",
    "Адам Кузгиев",
    "Анна Подгорная",
    "Тимур Дютин",
    "Вадим Воинцев",
    "Иван Роговский",
    "Анастасия Соболева",
    "Анна Сĸворцова",
    "Илья Иванов"
  ]
}];
