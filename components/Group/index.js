import styles from './styles.module.scss';

const Item = ({ mentors, tglink, interns }) => (
  <div className={styles.group}>
    <div className={styles.group_box}>
      <div className={styles.group_title}>Менторы</div>
      <ul className={styles.members}>
        {mentors.map((item, index) => (
          <li key={index}>
            <div className={styles.members_name}>{item.name}</div>
            <div className={styles.members_position}>{item.position}</div>
          </li>
        ))}
      </ul>
    </div>

    <div className={styles.group_box}>
      <div className={styles.group_title}>Канал связи</div>
      <div className={styles.members}>
        <div className={styles.group_tglink}>
          <a href={tglink} target="_blank"ц>Ссылка на канал telegram</a>
        </div>
      </div>
    </div>

    <div className={styles.group_box}>
      <div className={styles.group_title}>Интерны</div>
      <ul className={styles.members}>
        {interns.map((item, index) => (
          <li key={index}>
            <div className={styles.members_name}>{item}</div>
          </li>
        ))}
      </ul>
    </div>
  </div>
);

const Group = ({ list }) => (
  <ul className={styles.list}>
    {list.map((item, index) => (
      <li key={index}>
        <Item {...item} />
      </li>
    ))}
  </ul>
);

export default Group;
