import { useState } from 'react';

import { TABS_LIST } from '../../utils/variables';

import styles from './styles.module.scss';

const Tabs = ({ children }) => {
  const [activeTab, setActiveTab] = useState(0);

  return (
    <div className={styles.tabs}>
      <div className={styles.tabs_heading}>
        <ul className={styles.tabs_list}>
          {TABS_LIST.map((item, index) => (
            <li key={index}>
              <button
                onClick={() => setActiveTab(index)}
                className={`
                  ${styles.tabs_button}
                  ${styles[`tabs_button_${index === activeTab ? 'active' : 'disactive'}`]}
                `}
              >
                {item}
              </button>
            </li>
          ))}
        </ul>
      </div>
      <ul className={styles.tabs_list}>
        {children.map((item, index) => (
          <li
            className={styles[`tabs_${index === activeTab ? 'active' : 'disactive'}`]}
            key={index}
          >
            {item}
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Tabs;
