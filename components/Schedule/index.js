import styles from './styles.module.scss';

const Item = ({ date, time, mentor, tglink, lecture }) => (
  <div className={styles.schedule}>
    <div className={styles.schedule_title}>{lecture}</div>
    <ul className={styles.schedule_list}>
      <li className={styles.schedule_item}>
        <div className={styles.schedule_unit}>Дата: </div>
        <div className={styles.schedule_name}>{date}</div>
      </li>
      <li className={styles.schedule_item}>
        <div className={styles.schedule_unit}>Время: </div>
        <div className={styles.schedule_name}>{time}</div>
      </li>
      <li className={styles.schedule_item}>
        <div className={styles.schedule_unit}>Лектор: </div>
        <div className={styles.schedule_name}>{mentor}</div>
      </li>
      <li className={styles.schedule_item}>
        <div className={styles.schedule_unit}>Запись лекции: </div>
        <div className={styles.schedule_name}>
          <a href={tglink} target="_blank">Ссылка на канал telegram</a>
        </div>
      </li>
    </ul>
  </div>
);

const Schedule = ({ list }) => (
  <ul className={styles.list}>
    {list.map((item, index) => (
      <li key={index}>
        <Item {...item} />
      </li>
    ))}
  </ul>
);

export default Schedule;
