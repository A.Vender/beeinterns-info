import styles from './styles.module.scss';

const Heading = () => (
  <div className={styles.heading}>
    <div className={styles.heading_logo}>
      <img src="/beeinterns_logo.svg" alt="beeinterns" />
    </div>
  </div>
);

export default Heading;
